import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

export const onPostAdded = functions.firestore.document('posts/{newPostID}')
    .onCreate(async (snap, context) => {
        const newPost = snap.data();
        const postID = context.params.newPostID;
        let update_badge_rep_data = true

        if (newPost) {
            const user_id: string = newPost.user_id;
            const is_for_group: boolean = newPost.is_for_group;
            const is_for_competition: boolean = newPost.is_for_competition;
            const is_for_war: boolean = newPost.is_for_war;

            const time = Date.now()
            const promises = []
            if (is_for_competition) {
                const competition_id: string = newPost.competition_id
                try {
                    promises.push(admin.firestore().doc(`users/${user_id}`)
                        .update({
                            'memes_count': admin.firestore.FieldValue.increment(1)
                        }))

                    const querySnapshot = await admin.firestore().collection('posts')
                        .where('competition_id', '==', competition_id)
                        .where('user_id', '==', user_id)
                        .orderBy('create_date_time')
                        .limit(2)
                        .get()

                    console.log(querySnapshot.docs.length.toString())
                    if (querySnapshot.docs.length > 1) {
                        const shardId = Math.floor(Math.random() * 20);
                        const shardRef = admin.firestore().doc(`competitions/${competition_id}/submission_shards/${shardId.toString()}`);
                        promises.push(shardRef.set({ count: admin.firestore.FieldValue.increment(1) }, { merge: true }))
                    } else {
                        const shardId1 = Math.floor(Math.random() * 20);
                        const shardRef1 = admin.firestore().doc(`competitions/${competition_id}/submission_shards/${shardId1.toString()}`);
                        promises.push(shardRef1.set({ count: admin.firestore.FieldValue.increment(1) }, { merge: true }))

                        const shardId2 = Math.floor(Math.random() * 20);
                        const shardRef2 = admin.firestore().doc(`competitions/${competition_id}/participant_shards/${shardId2.toString()}`);
                        promises.push(shardRef2.set({ count: admin.firestore.FieldValue.increment(1) }, { merge: true }))
                    }

                    promises.push(admin.firestore().doc(`users_feed_data/${user_id}`)
                        .set({
                            'last_28_d': admin.firestore.FieldValue.arrayUnion({ 'post_id': postID, 'create_date_time': time }),
                            'last_updated': admin.firestore.FieldValue.serverTimestamp()
                        }, { merge: true }))
                } catch (error) {
                    console.log(error);
                    return error;
                }
            } else if (is_for_war) {
                const group_id: string = newPost.group_id;
                const war_id: string = newPost.war_id;

                promises.push(admin.firestore().doc(`groups/${group_id}`)
                    .update({
                        'memes_count': admin.firestore.FieldValue.increment(1)
                    }))
                promises.push(admin.firestore().doc(`wars/${war_id}`)
                    .update({
                        'memes_count': admin.firestore.FieldValue.increment(1)
                    }))

                promises.push(admin.firestore().doc(`groups_feed_data/${group_id}`)
                    .set({
                        'last_28_d': admin.firestore.FieldValue.arrayUnion({ 'post_id': postID, 'create_date_time': time }),
                        'last_updated': admin.firestore.FieldValue.serverTimestamp()
                    }, { merge: true }))
            } else if (is_for_group) {
                const is_approved: boolean = newPost.is_approved;

                if (is_approved) {
                    const group_id: string = newPost.group_id;
                    try {
                        promises.push(admin.firestore().doc(`groups/${group_id}`)
                            .update({
                                'memes_count': admin.firestore.FieldValue.increment(1)
                            }))

                        promises.push(admin.firestore().doc(`groups_feed_data/${group_id}`)
                            .set({
                                'last_28_d': admin.firestore.FieldValue.arrayUnion({ 'post_id': postID, 'create_date_time': time }),
                                'last_updated': admin.firestore.FieldValue.serverTimestamp()
                            }, { merge: true }))
                    } catch (error) {
                        console.log(error);
                        return error;
                    }
                } else {
                    update_badge_rep_data = false
                    const group_id: string = newPost.group_id;
                    try {
                        promises.push(admin.firestore().doc(`groups/${group_id}`)
                            .update({
                                'unapproved_memes_count': admin.firestore.FieldValue.increment(1)
                            }))

                        let group_name = ""
                        let user_name = ""
                        let first_name = ""
                        let last_name = ""

                        await admin.firestore().doc(`groups/${group_id}`).get().then(doc => {
                            group_name = Object(doc.data())["group_name"]
                        })

                        await admin.firestore().doc(`posts/${postID}`).get().then(doc => {
                            first_name = Object(doc.data())["first_name"]
                            last_name = Object(doc.data())["last_name"]
                            user_name = Object(doc.data())["user_name"]
                        })

                        const payload: admin.messaging.MessagingPayload = {
                            notification: {
                                title: group_name,
                                body: `${first_name} ${last_name} (@${user_name}) posted in ${group_name}\nTap to approve`,
                                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
                            },
                            data: {
                                "type": "group_approval_post",
                                "id": group_id
                            }
                        }

                        await admin.messaging().sendToTopic("group-" + group_id + "-admin", payload)
                    } catch (error) {
                        console.log(error);
                        return error;
                    }
                }
            } else {
                try {
                    promises.push(admin.firestore().doc(`users/${user_id}`)
                        .set({
                            'memes_count': admin.firestore.FieldValue.increment(1)
                        }, { merge: true }))

                    promises.push(admin.firestore().doc(`users_feed_data/${user_id}`)
                        .set({
                            'last_28_d': admin.firestore.FieldValue.arrayUnion({ 'post_id': postID, 'create_date_time': time }),
                            'last_updated': admin.firestore.FieldValue.serverTimestamp()
                        }, { merge: true }))
                } catch (error) {
                    console.log(error);
                    return error;
                }
            }

            if (update_badge_rep_data) {
                await updateBadgesForPosts(user_id)
            }

            return Promise.all(promises)
        } else {
            return null;
        }
    })

export const onReact = functions.firestore.document('posts/{postID}/reacts/{newreactID}')
    .onCreate(async (snap, context) => {
        const userID = snap.data()?.user_id
        const postID = context.params.postID;
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`posts/${postID}/react_shards/${shardId.toString()}`);

        const post = (await admin.firestore().doc(`posts/${postID}`).get()).data()
        const postOwnerUserID = Object(post)["user_id"]

        let user_name = ""
        let first_name = ""
        let last_name = ""
        let profile_pic = ""

        await admin.firestore().doc(`users/${userID}`).get().then(doc => {
            first_name = Object(doc.data())["first_name"]
            last_name = Object(doc.data())["last_name"]
            user_name = Object(doc.data())["user_name"]
            profile_pic = Object(doc.data())["profile_pic"]
        })

        const tokens = (await admin.firestore().collection(`users/${postOwnerUserID}/device_tokens`).get()).docs.map(token => token.id)

        const notification_body = `${first_name} ${last_name} (@${user_name}) reacted to your meme`

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: "Memecist Post",
                body: notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "post",
                "id": postID
            }
        }

        const promises: any = []

        promises.push(shardRef.set({ count: admin.firestore.FieldValue.increment(1) }, { merge: true }));

        promises.push(admin.firestore().collection(`users/${postOwnerUserID}/notifications`).add({
            'type': 'post',
            'id': postID,
            'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
            'message': notification_body,
            "leading_image": profile_pic
        }))

        promises.push(admin.firestore().doc(`users/${postOwnerUserID}/private_data/notification`).set(
            {
                'last_notification_time': admin.firestore.FieldValue.serverTimestamp()
            }, { merge: true }
        ))

        await Promise.all(promises)

        await updateBadgesForReacts(userID)

        return admin.messaging().sendToDevice(tokens, payload)
    })

export const onUnReact = functions.firestore.document('posts/{postID}/reacts/{reactID}')
    .onDelete(async (snap, context) => {
        const userID = snap.data()?.user_id
        const postID = context.params.postID;
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`posts/${postID}/react_shards/${shardId.toString()}`);

        const promises = []

        promises.push(shardRef.set({ count: admin.firestore.FieldValue.increment(-1) }, { merge: true }))

        promises.push(admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`)
            .set({
                'total_reacts': admin.firestore.FieldValue.increment(-1),
                'season_reacts': admin.firestore.FieldValue.increment(-1)
            }, { merge: true }))

        const badge_rep_data = (await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`).get()).data()

        const total_reacts = Object(badge_rep_data)["total_reacts"]

        if (total_reacts % 49 === 0 && total_reacts % 50 !== 0) {
            promises.push(admin.firestore().doc(`users/${userID}`)
                .set({
                    'rep': admin.firestore.FieldValue.increment(-0.01)
                }, { merge: true }))
        }

        return Promise.all(promises)
    })

export const onComment = functions.firestore.document('posts/{postID}/comments/{commentID}')
    .onCreate(async (snap, context) => {
        const userID = snap.data()?.user_id
        const postID = context.params.postID
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`posts/${postID}/comment_shards/${shardId.toString()}`);

        const post = (await admin.firestore().doc(`posts/${postID}`).get()).data()
        const postOwnerUserID = Object(post)["user_id"]

        let user_name = ""
        let first_name = ""
        let last_name = ""
        let profile_pic = ""

        await admin.firestore().doc(`users/${userID}`).get().then(doc => {
            first_name = Object(doc.data())["first_name"]
            last_name = Object(doc.data())["last_name"]
            user_name = Object(doc.data())["user_name"]
            profile_pic = Object(doc.data())["profile_pic"]
        })

        const tokens = (await admin.firestore().collection(`users/${postOwnerUserID}/device_tokens`).get()).docs.map(token => token.id)

        const notification_body = `${first_name} ${last_name} (@${user_name}) commented to your meme`

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: "Memecist Post",
                body: notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "post",
                "id": postID
            }
        }

        const promises: any = []

        promises.push(shardRef.set({ count: admin.firestore.FieldValue.increment(1) }, { merge: true }))

        promises.push(admin.firestore().collection(`users/${postOwnerUserID}/notifications`).add({
            'type': 'post',
            'id': postID,
            'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
            'message': notification_body,
            "leading_image": profile_pic
        }))

        promises.push(admin.firestore().doc(`users/${postOwnerUserID}/private_data/notification`).set(
            {
                'last_notification_time': admin.firestore.FieldValue.serverTimestamp()
            }, { merge: true }
        ))

        await Promise.all(promises)

        await updateBadgesForComments(userID)

        return admin.messaging().sendToDevice(tokens, payload)
    })

export const onDeleteComment = functions.firestore.document('posts/{postID}/comments/{commentID}')
    .onDelete(async (snap, context) => {
        const userID = snap.data()?.user_id
        const postID = context.params.postID
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`posts/${postID}/comment_shards/${shardId.toString()}`);

        const promises = []

        promises.push(shardRef.set({ count: admin.firestore.FieldValue.increment(-1) }, { merge: true }))

        promises.push(admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`)
            .set({
                'total_comments': admin.firestore.FieldValue.increment(-1),
                'season_comments': admin.firestore.FieldValue.increment(-1)
            }, { merge: true }))


        const badge_rep_data = (await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`).get()).data()

        const total_comments = Object(badge_rep_data)["total_comments"]

        if (total_comments % 49 === 0 && total_comments % 50 !== 0) {
            promises.push(admin.firestore().doc(`users/${userID}`)
                .set({
                    'rep': admin.firestore.FieldValue.increment(-0.03)
                }, { merge: true }))
        }

        return Promise.all(promises)
    })

export const approveMeme = functions.https.onCall(async (data, context) => {
    const userID = context.auth?.uid
    const postID = data.post_id
    const groupID = data.group_id

    const time = Date.now()

    return admin.firestore().collection(`groups/${groupID}/group_admins`)
        .where('user_id', "==", userID)
        .get()
        .then((snapshot) => {
            if (snapshot.docs.length === 1) {
                return admin.firestore().doc(`posts/${postID}`).get().then(async value => {
                    if (value.exists) {
                        if (value?.data()?.group_id === groupID) {
                            const postOwnerUserID = Object(value.data())["user_id"]
                            const tokens = (await admin.firestore().collection(`users/${postOwnerUserID}/device_tokens`).get()).docs.map(token => token.id)

                            let group_name = ""

                            await admin.firestore().doc(`groups/${groupID}`).get().then(doc => {
                                group_name = Object(doc.data())["group_name"]
                            })

                            const payload: admin.messaging.MessagingPayload = {
                                notification: {
                                    title: "Post Approved!",
                                    body: `Your post in ${group_name} is approved`,
                                    clickAction: 'FLUTTER_NOTIFICATION_CLICK'
                                },
                                data: {
                                    "type": "post",
                                    "id": postID
                                }
                            }

                            const batch = admin.firestore().batch()

                            batch.update(admin.firestore().doc(`posts/${postID}`), {
                                'approved_by': userID,
                                'is_approved': true,
                                'approve_date_time': admin.firestore.FieldValue.serverTimestamp()
                            })

                            batch.update(admin.firestore().doc(`groups/${groupID}`), {
                                'memes_count': admin.firestore.FieldValue.increment(1),
                                'unapproved_memes_count': admin.firestore.FieldValue.increment(-1)
                            })

                            batch.set(admin.firestore().doc(`groups_feed_data/${groupID}`), {
                                'last_28_d': admin.firestore.FieldValue.arrayUnion({ 'post_id': postID, 'create_date_time': time })
                            }, { merge: true })

                            await batch.commit()

                            await updateBadgesForPosts(postOwnerUserID)

                            return admin.messaging().sendToDevice(tokens, payload)
                        } else {
                            return null
                        }
                    } else {
                        return null
                    }
                })
            } else {
                return null
            }
        })

})

export const sharePost = functions.https.onCall(async (data, context) => {
    const userID = context.auth?.uid
    const postID = data.post_id
    const link = data.link
    const promises = []

    if (userID != null) {

        await updateBadgesForShares(userID)

        promises.push(admin.firestore().collection(`users/${userID}/post_shares`).add({
            'post_id': postID,
            'link': link,
            'create_date_time': admin.firestore.FieldValue.serverTimestamp()
        }))

        return Promise.all(promises)
    } else {
        return null
    }
})

export const onPostReported = functions.firestore.document('posts/{postID}/reports/{reportID}')
    .onCreate(async (snap, context) => {
        const postID = context.params.postID

        const shardId = Math.floor(Math.random() * 3);
        const shardRef = admin.firestore().doc(`posts/${postID}/report_shards/${shardId.toString()}`);
        return shardRef.set({ count: admin.firestore.FieldValue.increment(1) }, { merge: true })
    })

//TODO decide if to return boolean for each https on call functions
//TODO remove from notifications and feed collection or on client side show failed to get post
export const deletePost = functions.https.onCall(async (data, context) => {
    const postID = data.post_id
    const userID = context?.auth?.uid

    const post = (await admin.firestore().doc(`posts/${postID}`).get()).data();

    if (Object(post)["user_id"] === userID) {
        const deleteBatch = admin.firestore().batch()

        const deletedPost = {
            "caption": Object(post)["caption"],
            "category": Object(post)["category"],
            "comments_count": Object(post)["comments_count"],
            "create_date_time": Object(post)["create_date_time"],
            "first_name": Object(post)["first_name"],
            "hashtags": Object(post)["hashtags"],
            "image_text": Object(post)["image_text"],
            "is_for_competition": Object(post)["is_for_competition"],
            "is_for_group": Object(post)["is_for_group"],
            "is_for_war": Object(post)["is_for_war"],
            "is_image_text_extracted": Object(post)["is_image_text_extracted"],
            "is_verified": Object(post)["is_verified"],
            "last_name": Object(post)["last_name"],
            "media_url": Object(post)["media_url"],
            "post_id": Object(post)["post_id"],
            "post_type": Object(post)["post_type"],
            "profile_pic": Object(post)["profile_pic"],
            "react_count": Object(post)["react_count"],
            "search_keywords": Object(post)["search_keywords"],
            "user_id": Object(post)["user_id"],
            "user_name": Object(post)["user_name"],
            "deleted_date_time": admin.firestore.FieldValue.serverTimestamp()
        }

        if (Object(post)["competition_id"] !== null)
            Object(deletedPost)["competition_id"] = Object(post)["competition_id"]

        if (Object(post)["war_id"] !== null)
            Object(deletedPost)["war_id"] = Object(post)["war_id"]

        if (Object(post)["group_id"] !== null)
            Object(deletedPost)["group_id"] = Object(post)["group_id"]

        deleteBatch.set(admin.firestore().doc(`deleted_posts/${postID}_deleted`), deletedPost)

        deleteBatch.delete(admin.firestore().doc(`posts/${postID}`))

        return deleteBatch.commit()
    } else {
        return null
    }
})

const updateBadgesForPosts = async (user_id: string) => {
    await admin.firestore().doc(`users/${user_id}/private_data/badge_rep_data`)
        .set({
            'total_posts': admin.firestore.FieldValue.increment(1),
            'season_posts': admin.firestore.FieldValue.increment(1)
        }, { merge: true })

    //Badge update functionality
    const badge_rep_data = (await admin.firestore().doc(`users/${user_id}/private_data/badge_rep_data`).get()).data()

    const season_posts = Object(badge_rep_data)["season_posts"]

    await admin.firestore().doc(`users/${user_id}`)
        .set({
            'rep': admin.firestore.FieldValue.increment(0.05)
        }, { merge: true })


    let is_badge_updated = false
    let new_badge_notification_body = ""

    if (season_posts === 60) {
        await admin.firestore().doc(`users/${user_id}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fnewbie.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${user_id}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fmemer.png?alt=media')
            }, { merge: true })


        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Memer Badge!'
        is_badge_updated = true
    } else if (season_posts === 120) {
        await admin.firestore().doc(`users/${user_id}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fmemer.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${user_id}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fmeme_lord.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Meme Lord Badge!'
        is_badge_updated = true
    } else if (season_posts === 180) {
        await admin.firestore().doc(`users/${user_id}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fmeme_lord.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${user_id}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fmeme_whiz.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Meme Whiz Badge!'
        is_badge_updated = true
    } else if (season_posts === 360) {
        await admin.firestore().doc(`users/${user_id}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fmeme_whiz.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${user_id}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fmeme_guru.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Meme Guru Badge!'
        is_badge_updated = true
    }

    if (is_badge_updated) {
        await admin.firestore().collection(`users/${user_id}/notifications`).add({
            'type': 'user',
            'id': user_id,
            'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
            'message': new_badge_notification_body,
            "leading_image": "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fbadge.png?alt=media"
        })

        await admin.firestore().doc(`users/${user_id}/private_data/notification`).set(
            {
                'last_notification_time': admin.firestore.FieldValue.serverTimestamp()
            }, { merge: true }
        )

        const payload_badge: admin.messaging.MessagingPayload = {
            notification: {
                title: "New Badge!!",
                body: new_badge_notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "user",
                "id": user_id
            }
        }

        const tokens_for_badge_notif = (await admin.firestore().collection(`users/${user_id}/device_tokens`).get()).docs.map(token => token.id)

        await admin.messaging().sendToDevice(tokens_for_badge_notif, payload_badge)
    }
}

const updateBadgesForReacts = async (userID: string) => {
    await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`)
        .set({
            'total_reacts': admin.firestore.FieldValue.increment(1),
            'season_reacts': admin.firestore.FieldValue.increment(1)
        }, { merge: true })

    //Badge update functionality
    const badge_rep_data = (await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`).get()).data()

    const total_reacts = Object(badge_rep_data)["total_reacts"]
    const season_reacts = Object(badge_rep_data)["season_reacts"]

    if (total_reacts % 50 === 0) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'rep': admin.firestore.FieldValue.increment(0.01)
            }, { merge: true })
    }

    let is_badge_updated = false
    let new_badge_notification_body = ""

    //TODO check if the expression after === is not skipped and decide if want to use >=
    if (season_reacts === 100) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fnewbie.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Freact_whiz.png?alt=media')
            }, { merge: true })


        new_badge_notification_body = 'Congratulations!!🎉\nYou got the React Whiz Badge!'
        is_badge_updated = true
    } else if (season_reacts === 300) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Freact_whiz.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Freact_master.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the React Master Badge!'
        is_badge_updated = true
    } else if (season_reacts === 600) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Freact_master.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Freact_star.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the React Star Badge!'
        is_badge_updated = true
    } else if (season_reacts === 1000) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Freact_star.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Freact_guru.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the React Guru Badge!'
        is_badge_updated = true
    }

    if (is_badge_updated) {
        await admin.firestore().collection(`users/${userID}/notifications`).add({
            'type': 'user',
            'id': userID,
            'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
            'message': new_badge_notification_body,
            "leading_image": "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fbadge.png?alt=media"
        })

        await admin.firestore().doc(`users/${userID}/private_data/notification`).set(
            {
                'last_notification_time': admin.firestore.FieldValue.serverTimestamp()
            }, { merge: true }
        )

        const payload_badge: admin.messaging.MessagingPayload = {
            notification: {
                title: "New Badge!!",
                body: new_badge_notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "user",
                "id": userID
            }
        }

        const tokens_for_badge_notif = (await admin.firestore().collection(`users/${userID}/device_tokens`).get()).docs.map(token => token.id)

        await admin.messaging().sendToDevice(tokens_for_badge_notif, payload_badge)
    }
}

const updateBadgesForComments = async (userID: string) => {
    await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`)
        .set({
            'total_comments': admin.firestore.FieldValue.increment(1),
            'season_comments': admin.firestore.FieldValue.increment(1)
        }, { merge: true })

    //Badge update functionality
    const badge_rep_data = (await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`).get()).data()

    const total_comments = Object(badge_rep_data)["total_comments"]
    const season_comments = Object(badge_rep_data)["season_comments"]

    if (total_comments % 50 === 0) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'rep': admin.firestore.FieldValue.increment(0.03)
            }, { merge: true })
    }

    let is_badge_updated = false
    let new_badge_notification_body = ""

    if (season_comments === 60) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fnewbie.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fcomment_whiz.png?alt=media')
            }, { merge: true })


        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Comment Whiz Badge!'
        is_badge_updated = true
    } else if (season_comments === 120) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fcomment_whiz.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fcomment_master.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Comment Master Badge!'
        is_badge_updated = true
    } else if (season_comments === 180) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fcomment_master.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fcomment_star.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Comment Star Badge!'
        is_badge_updated = true
    } else if (season_comments === 360) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fcomment_star.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fcomment_guru.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Comment Guru Badge!'
        is_badge_updated = true
    }

    if (is_badge_updated) {
        await admin.firestore().collection(`users/${userID}/notifications`).add({
            'type': 'user',
            'id': userID,
            'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
            'message': new_badge_notification_body,
            "leading_image": "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fbadge.png?alt=media"
        })

        await admin.firestore().doc(`users/${userID}/private_data/notification`).set(
            {
                'last_notification_time': admin.firestore.FieldValue.serverTimestamp()
            }, { merge: true }
        )

        const payload_badge: admin.messaging.MessagingPayload = {
            notification: {
                title: "New Badge!!",
                body: new_badge_notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "user",
                "id": userID
            }
        }

        const tokens_for_badge_notif = (await admin.firestore().collection(`users/${userID}/device_tokens`).get()).docs.map(token => token.id)

        await admin.messaging().sendToDevice(tokens_for_badge_notif, payload_badge)
    }
}

const updateBadgesForShares = async (userID: string) => {
    await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`)
        .set({
            'total_shares': admin.firestore.FieldValue.increment(1),
            'season_shares': admin.firestore.FieldValue.increment(1)
        }, { merge: true })

    //Badge update functionality
    const badge_rep_data = (await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`).get()).data()

    const total_shares = Object(badge_rep_data)["total_shares"]
    const season_shares = Object(badge_rep_data)["season_shares"]

    if (total_shares % 50 === 0) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'rep': admin.firestore.FieldValue.increment(0.02)
            }, { merge: true })
    }

    let is_badge_updated = false
    let new_badge_notification_body = ""

    if (season_shares === 60) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fnewbie.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fshare_whiz.png?alt=media')
            }, { merge: true })


        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Share Whiz Badge!'
        is_badge_updated = true
    } else if (season_shares === 120) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fshare_whiz.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fshare_master.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Share Master Badge!'
        is_badge_updated = true
    } else if (season_shares === 180) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fshare_master.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fshare_star.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Share Star Badge!'
        is_badge_updated = true
    } else if (season_shares === 300) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fshare_star.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fshare_guru.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Share Guru Badge!'
        is_badge_updated = true
    }

    if (is_badge_updated) {
        await admin.firestore().collection(`users/${userID}/notifications`).add({
            'type': 'user',
            'id': userID,
            'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
            'message': new_badge_notification_body,
            "leading_image": "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fbadge.png?alt=media"
        })

        await admin.firestore().doc(`users/${userID}/private_data/notification`).set(
            {
                'last_notification_time': admin.firestore.FieldValue.serverTimestamp()
            }, { merge: true }
        )

        const payload_badge: admin.messaging.MessagingPayload = {
            notification: {
                title: "New Badge!!",
                body: new_badge_notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "user",
                "id": userID
            }
        }

        const tokens_for_badge_notif = (await admin.firestore().collection(`users/${userID}/device_tokens`).get()).docs.map(token => token.id)

        await admin.messaging().sendToDevice(tokens_for_badge_notif, payload_badge)
    }
}