import * as admin from 'firebase-admin';

admin.initializeApp({
    credential: admin.credential.cert(require('../keys/admin.json'))
})

export * from './groups';
export * from './users';
export * from './posts';
export * from './wars';
export * from './feed';