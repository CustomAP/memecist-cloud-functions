import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';


export const onGroupCreated = functions.firestore.document('groups/{groupID}')
    .onCreate(async (snap, context) => {
        const groupID = context.params.groupID

        return admin.firestore().doc(`groups/${groupID}/group_data/group_details`)
            .set(
                {
                    'last_name_change_date_time': admin.firestore.Timestamp.fromDate(new Date(1970, 1, 1, 0, 0, 0)),
                    'changes_history': [{
                        'create_date_time': snap.data().create_date_time,
                        'group_name': snap.data().group_name,
                        'group_description': snap.data().group_description,
                        'group_profile_pic': snap.data().group_profile_pic
                    }]
                }
            )
    })

export const onAdminAddedToGroup = functions.firestore.document('groups/{groupID}/group_admins/{adminID}')
    .onCreate(async (snap, context) => {
        const groupID = context.params.groupID;
        const userID = context.params.adminID;
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`groups/${groupID}/member_shards/${shardId.toString()}`);

        const tokens = (await admin.firestore().collection(`users/${userID}/device_tokens`).get()).docs.map(snapshot => snapshot.id)

        //TODO decide if to send a notification

        const last_28_d_posts_of_group = await admin.firestore().doc(`groups_feed_data/${groupID}`).get()
        const feed_of_user = await admin.firestore().doc(`users/${userID}/private_data/feed`).get()

        const new_posts: any[] = Object(last_28_d_posts_of_group.data())["last_28_d"]

        let posts: Map<any, any>[] = Object(feed_of_user.data())["posts"]

        if (new_posts && posts) {
            new_posts.forEach(post => {
                posts.push(post)
            })

            posts = posts.sort((a, b) => Object(b)["create_date_time"] - Object(a)["create_date_time"])

            posts = posts.slice(0, 300)

            const posts_count = posts.length;

            await admin.firestore().doc(`users/${userID}/private_data/feed`).update({ 'posts': posts, 'posts_count': posts_count })
        }

        await admin.messaging().subscribeToTopic(tokens, "group-" + groupID + "-admin")
        await admin.messaging().subscribeToTopic(tokens, "group-" + groupID)
        return shardRef.set({ count: admin.firestore.FieldValue.increment(1) }, { merge: true })
    })

export const onMemberAddedToGroup = functions.firestore.document('groups/{groupID}/group_members/{memberID}')
    .onCreate(async (snap, context) => {
        const userID = context.params.memberID
        const groupID = context.params.groupID;
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`groups/${groupID}/member_shards/${shardId.toString()}`);

        let group_name = ""
        let user_name = ""
        let first_name = ""
        let last_name = ""

        await admin.firestore().doc(`users/${userID}`).get().then(doc => {
            first_name = Object(doc.data())["first_name"]
            last_name = Object(doc.data())["last_name"]
            user_name = Object(doc.data())["user_name"]
        })

        await admin.firestore().doc(`groups/${groupID}`).get().then(doc => {
            group_name = Object(doc.data())["group_name"]
        })

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: group_name,
                body: `${first_name} ${last_name} (@${user_name}) joined the group.`,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "group",
                "id": groupID
            }
        }

        await shardRef.set({ count: admin.firestore.FieldValue.increment(1) }, { merge: true });

        const last_28_d_posts_of_group = await admin.firestore().doc(`groups_feed_data/${groupID}`).get()
        const feed_of_user = await admin.firestore().doc(`users/${userID}/private_data/feed`).get()

        const new_posts: any[] = Object(last_28_d_posts_of_group.data())["last_28_d"]

        let posts: Map<any, any>[] = Object(feed_of_user.data())["posts"]

        if (new_posts && posts) {
            new_posts.forEach(post => {
                posts.push(post)
            })

            posts = posts.sort((a, b) => Object(b)["create_date_time"] - Object(a)["create_date_time"])

            posts = posts.slice(0, 300)

            const posts_count = posts.length;

            await admin.firestore().doc(`users/${userID}/private_data/feed`).update({ 'posts': posts, 'posts_count': posts_count })
        }

        return admin.messaging().sendToTopic("group-" + groupID + "-admin", payload)
    })

export const onMemberLeft = functions.firestore.document('groups/{groupID}/group_members/{memberID}')
    .onDelete(async (snap, context) => {
        const groupID = context.params.groupID;
        const userID = context.params.memberID
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`groups/${groupID}/member_shards/${shardId.toString()}`);

        const tokens = (await admin.firestore().collection(`users/${userID}/device_tokens`).get()).docs.map(snapshot => snapshot.id)

        await admin.messaging().unsubscribeFromTopic(tokens, "group-" + groupID)

        const last_28_d_posts_of_group = await admin.firestore().doc(`groups_feed_data/${groupID}`).get()
        const feed_of_user = await admin.firestore().doc(`users/${userID}/private_data/feed`).get()

        const new_posts: any[] = Object(last_28_d_posts_of_group.data())["last_28_d"]

        let posts: Map<any, any>[] = Object(feed_of_user.data())["posts"]

        if (new_posts && posts) {
            for (let i = 0; i < new_posts.length; i++) {
                for (let j = 0; j < posts.length; j++) {
                    if (Object(new_posts[i])["post_id"] === Object(posts[j])["post_id"]) {
                        posts.splice(j, 1)
                        j--
                    }
                }
            }

            posts = posts.sort((a, b) => Object(b)["create_date_time"] - Object(a)["create_date_time"])

            const posts_count = posts.length

            await admin.firestore().doc(`users/${userID}/private_data/feed`).update({ 'posts': posts, 'posts_count': posts_count })
        }

        return shardRef.set({ count: admin.firestore.FieldValue.increment(-1) }, { merge: true });
    })

export const onAdminLeft = functions.firestore.document('groups/{groupID}/group_admins/{adminID}')
    .onDelete(async (snap, context) => {
        const groupID = context.params.groupID;
        const userID = context.params.adminID;
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`groups/${groupID}/member_shards/${shardId.toString()}`);

        const tokens = (await admin.firestore().collection(`users/${userID}/device_tokens`).get()).docs.map(snapshot => snapshot.id)

        await admin.messaging().unsubscribeFromTopic(tokens, "group-" + groupID + "-admin")
        await admin.messaging().unsubscribeFromTopic(tokens, "group-" + groupID)

        const last_28_d_posts_of_group = await admin.firestore().doc(`groups_feed_data/${groupID}`).get()
        const feed_of_user = await admin.firestore().doc(`users/${userID}/private_data/feed`).get()

        const new_posts: any[] = Object(last_28_d_posts_of_group.data())["last_28_d"]

        let posts: Map<any, any>[] = Object(feed_of_user.data())["posts"]

        if (new_posts && posts) {
            for (let i = 0; i < new_posts.length; i++) {
                for (let j = 0; j < posts.length; j++) {
                    if (Object(new_posts[i])["post_id"] === Object(posts[j])["post_id"]) {
                        posts.splice(j, 1)
                        j--
                    }
                }
            }

            posts = posts.sort((a, b) => Object(b)["create_date_time"] - Object(a)["create_date_time"])

            const posts_count = posts.length

            await admin.firestore().doc(`users/${userID}/private_data/feed`).update({ 'posts': posts, 'posts_count': posts_count })
        }

        return shardRef.set({ count: admin.firestore.FieldValue.increment(-1) }, { merge: true });
    })

export const updateGroupMembersCount = functions.https.onCall(async (data, context) => {
    const groupID = data.group_id

    return admin.firestore().collection(`groups/${groupID}/member_shards`)
        .get()
        .then(querySnapshot => {
            let count: number = 0

            querySnapshot.docs.forEach(doc => {
                count += Object(doc.data())["count"]
            })

            return admin.firestore().doc(`groups/${groupID}`)
                .update({
                    'members_count': count
                })
        })
})

export const editGroupDetails = functions.https.onCall(async (data, context) => {
    const groupID = data.group_id ?? null
    const groupName = data.group_name ?? null
    const groupDescription = data.group_description ?? null
    const groupProfilePic = data.group_profile_pic ?? null
    const searchKeywords = data.search_keywords ?? null
    const userID = context?.auth?.uid

    try {
        if (userID !== null) {
            const docSnapshot = await admin.firestore().doc(`groups/${groupID}/group_admins/${userID}`).get()

            if (docSnapshot !== null) {
                if (groupName !== null && groupName !== "") {
                    await admin.firestore().doc(`groups/${groupID}`).update({ 'group_name': groupName })
                    await admin.firestore().doc(`groups/${groupID}/group_data/group_details`).update(
                        {
                            'changes_history': admin.firestore.FieldValue.arrayUnion(
                                {
                                    'create_date_time': Date.now(),
                                    'group_name': groupName
                                }
                            ),
                            'last_name_change_date_time': admin.firestore.FieldValue.serverTimestamp()
                        }
                    )

                    const batchArray: any[] = []
                    batchArray.push(admin.firestore().batch());
                    let operationCounter = 0
                    let batchIndex = 0

                    const postSnapshotArray = await admin.firestore().collection(`posts`).where('group_id', '==', groupID).get()

                    postSnapshotArray.forEach(snapshot => {
                        batchArray[batchIndex].update(snapshot.ref, { 'group_name': groupName })
                        operationCounter++;

                        if (operationCounter === 499) {
                            batchArray.push(admin.firestore().batch());
                            batchIndex++;
                            operationCounter = 0;
                        }
                    })

                    const warSnapshotArray = await admin.firestore().collection(`wars`).where('group_id', '==', groupID).get()

                    warSnapshotArray.forEach(snapshot => {
                        batchArray[batchIndex].update(snapshot.ref, { 'group_name': groupName })
                        operationCounter++;

                        if (operationCounter === 499) {
                            batchArray.push(admin.firestore().batch());
                            batchIndex++;
                            operationCounter = 0;
                        }
                    })

                    batchArray.forEach(async batch => await batch.commit());
                }

                if (searchKeywords !== null && searchKeywords !== "") {
                    await admin.firestore().doc(`groups/${groupID}`).update({ 'search_keywords': searchKeywords })
                }

                if (groupDescription !== null && groupDescription !== "") {
                    await admin.firestore().doc(`groups/${groupID}`).update({ 'group_description': groupDescription })
                    await admin.firestore().doc(`groups/${groupID}/group_data/group_details`).update(
                        {
                            'changes_history': admin.firestore.FieldValue.arrayUnion(
                                {
                                    'create_date_time': Date.now(),
                                    'group_description': groupDescription
                                }
                            )
                        }
                    )
                }

                if (groupProfilePic !== null && groupProfilePic !== "") {
                    await admin.firestore().doc(`groups/${groupID}`).update({ 'group_profile_pic': groupProfilePic })
                    await admin.firestore().doc(`groups/${groupID}/group_data/group_details`).update(
                        {
                            'changes_history': admin.firestore.FieldValue.arrayUnion(
                                {
                                    'create_date_time': Date.now(),
                                    'group_profile_pic': groupProfilePic
                                }
                            )
                        }
                    )
                }

                return { 'result': true }
            } else {
                return { 'result': false }
            }
        } else {
            return { "result": false }
        }
    } catch (e) {
        console.log(e)
        return { "result": false }
    }
})