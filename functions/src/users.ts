import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

export const onFollow = functions.firestore.document('users/{userID}/following/{followingUserID}')
    .onCreate(async (snap, context) => {
        const userID = context.params.userID
        const followingUserID = context.params.followingUserID;
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`users/${followingUserID}/follower_shards/${shardId.toString()}`);

        let user_name = ""
        let first_name = ""
        let last_name = ""
        let profile_pic = ""

        await admin.firestore().doc(`users/${userID}`).get().then(doc => {
            first_name = Object(doc.data())["first_name"]
            last_name = Object(doc.data())["last_name"]
            user_name = Object(doc.data())["user_name"]
            profile_pic = Object(doc.data())["profile_pic"]
        })

        const tokens = (await admin.firestore().collection(`users/${followingUserID}/device_tokens`).get()).docs.map(token => token.id)

        const notification_body = `${first_name} ${last_name} (@${user_name}) started following you`

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: `${first_name} ${last_name}`,
                body: notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "user",
                "id": userID
            }
        }

        await shardRef.set({ count: admin.firestore.FieldValue.increment(1) }, { merge: true });

        await admin.firestore().collection(`users/${followingUserID}/notifications`).add({
            'type': 'user',
            'id': userID,
            'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
            'message': notification_body,
            "leading_image": profile_pic
        })

        await admin.firestore().doc(`users/${followingUserID}/private_data/notification`).set(
            {
                'last_notification_time': admin.firestore.FieldValue.serverTimestamp()
            }, { merge: true }
        )

        const last_28_d_posts_of_following_user = await admin.firestore().doc(`users_feed_data/${followingUserID}`).get()
        const feed_of_user = await admin.firestore().doc(`users/${userID}/private_data/feed`).get()

        const new_posts: any[] = Object(last_28_d_posts_of_following_user.data())["last_28_d"]

        let posts: Map<any, any>[] = Object(feed_of_user.data())["posts"]

        if (new_posts && posts) {
            new_posts.forEach(post => {
                posts.push(post)
            })

            posts = posts.sort((a, b) => Object(b)["create_date_time"] - Object(a)["create_date_time"])

            posts = posts.slice(0, 300)

            const posts_count = posts.length;

            await admin.firestore().doc(`users/${userID}/private_data/feed`).update({ 'posts': posts, 'posts_count': posts_count })
        }

        return admin.messaging().sendToDevice(tokens, payload)
    })

export const onUnFollow = functions.firestore.document('users/{userID}/following/{followingUserID}')
    .onDelete(async (snap, context) => {
        const followingUserID = context.params.followingUserID;
        const userID = context.params.userID;
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`users/${followingUserID}/follower_shards/${shardId.toString()}`);

        const last_28_d_posts_of_following_user = await admin.firestore().doc(`users_feed_data/${followingUserID}`).get()
        const feed_of_user = await admin.firestore().doc(`users/${userID}/private_data/feed`).get()

        const new_posts: any[] = Object(last_28_d_posts_of_following_user.data())["last_28_d"]

        let posts: Map<any, any>[] = Object(feed_of_user.data())["posts"]

        if (new_posts && posts) {
            for (let i = 0; i < new_posts.length; i++) {
                for (let j = 0; j < posts.length; j++) {
                    if (Object(new_posts[i])["post_id"] === Object(posts[j])["post_id"]) {
                        posts.splice(j, 1)
                        j--
                    }
                }
            }

            posts = posts.sort((a, b) => Object(b)["create_date_time"] - Object(a)["create_date_time"])

            const posts_count = posts.length

            await admin.firestore().doc(`users/${userID}/private_data/feed`).update({ 'posts': posts, 'posts_count': posts_count })
        }

        return shardRef.set({ count: admin.firestore.FieldValue.increment(-1) }, { merge: true });
    })

export const updateFollowersCount = functions.https.onCall(async (data, context) => {
    const userID = data.user_id

    return admin.firestore().collection(`users/${userID}/follower_shards`)
        .get()
        .then(querySnapshot => {
            let count: number = 0

            querySnapshot.docs.forEach(doc => {
                count += Object(doc.data())["count"]
            })

            return admin.firestore().doc(`users/${userID}`)
                .update({
                    'followers_count': count
                })
        })
})

//not updating notifications and war-created by field
export const editProfileInfo = functions.https.onCall(async (data, context) => {
    const userName = data.user_name
    const firstName = data.first_name
    const lastName = data.last_name
    const searchKeywords = data.search_keywords
    const profilePic = data.profile_pic
    const userID = context?.auth?.uid

    if (userID !== null) {
        try {
            if (userName !== null && firstName !== null && lastName !== null && searchKeywords !== null && profilePic !== null) {
                const batchArray: any[] = []
                batchArray.push(admin.firestore().batch());
                let operationCounter = 0
                let batchIndex = 0

                batchArray[batchIndex].update(admin.firestore()
                    .doc(`users/${userID}`),
                    {
                        'user_name': userName,
                        'first_name': firstName,
                        'last_name': lastName,
                        'profile_pic': profilePic
                    })
                operationCounter++

                batchArray[batchIndex].update(admin.firestore()
                    .doc(`users/${userID}/private_data/user_details`),
                    {
                        'changes_history': admin.firestore.FieldValue.arrayUnion(
                            {
                                'create_date_time': Date.now(),
                                'user_name': userName,
                                'first_name': firstName,
                                'last_name': lastName,
                                'search_keywords': searchKeywords,
                                'profile_pic': profilePic
                            }
                        ),
                        'last_profile_edit_date_time': admin.firestore.FieldValue.serverTimestamp()
                    })
                operationCounter++

                const postSnapshotArray = await admin.firestore().collection(`posts`).where('user_id', '==', userID).get()

                postSnapshotArray.forEach(snapshot => {
                    batchArray[batchIndex].update(snapshot.ref, {
                        'user_name': userName,
                        'first_name': firstName,
                        'last_name': lastName,
                        'profile_pic': profilePic
                    })
                    operationCounter++;

                    if (operationCounter === 499) {
                        batchArray.push(admin.firestore().batch());
                        batchIndex++;
                        operationCounter = 0;
                    }
                })

                const followingSnapshotArray = await admin.firestore().collectionGroup(`following`).where('user_id', '==', userID).get()

                followingSnapshotArray.forEach(snapshot => {
                    batchArray[batchIndex].update(snapshot.ref, {
                        'user_name': userName,
                        'first_name': firstName,
                        'last_name': lastName,
                        'search_keywords': searchKeywords,
                        'profile_pic': profilePic
                    })
                    operationCounter++;

                    if (operationCounter === 499) {
                        batchArray.push(admin.firestore().batch());
                        batchIndex++;
                        operationCounter = 0;
                    }
                })

                const groupAdminsSnapshotArray = await admin.firestore().collectionGroup(`group_admins`).where('user_id', '==', userID).get()

                groupAdminsSnapshotArray.forEach(snapshot => {
                    batchArray[batchIndex].update(snapshot.ref, {
                        'user_name': userName,
                        'first_name': firstName,
                        'last_name': lastName,
                        'search_keywords': searchKeywords,
                        'profile_pic': profilePic
                    })
                    operationCounter++;

                    if (operationCounter === 499) {
                        batchArray.push(admin.firestore().batch());
                        batchIndex++;
                        operationCounter = 0;
                    }
                })

                const groupMembersSnapshotArray = await admin.firestore().collectionGroup(`group_members`).where('user_id', '==', userID).get()

                groupMembersSnapshotArray.forEach(snapshot => {
                    batchArray[batchIndex].update(snapshot.ref, {
                        'user_name': userName,
                        'first_name': firstName,
                        'last_name': lastName,
                        'search_keywords': searchKeywords,
                        'profile_pic': profilePic
                    })
                    operationCounter++;

                    if (operationCounter === 499) {
                        batchArray.push(admin.firestore().batch());
                        batchIndex++;
                        operationCounter = 0;
                    }
                })

                const reactsSnapshotArray = await admin.firestore().collectionGroup(`reacts`).where('user_id', '==', userID).get()

                reactsSnapshotArray.forEach(snapshot => {
                    batchArray[batchIndex].update(snapshot.ref, {
                        'user_name': userName,
                        'first_name': firstName,
                        'last_name': lastName,
                        'search_keywords': searchKeywords,
                        'profile_pic': profilePic
                    })
                    operationCounter++;

                    if (operationCounter === 499) {
                        batchArray.push(admin.firestore().batch());
                        batchIndex++;
                        operationCounter = 0;
                    }
                })

                const commentsSnapshotArray = await admin.firestore().collectionGroup(`comments`).where('user_id', '==', userID).get()

                commentsSnapshotArray.forEach(snapshot => {
                    batchArray[batchIndex].update(snapshot.ref, {
                        'user_name': userName,
                        'first_name': firstName,
                        'last_name': lastName,
                        'profile_pic': profilePic
                    })
                    operationCounter++;

                    if (operationCounter === 499) {
                        batchArray.push(admin.firestore().batch());
                        batchIndex++;
                        operationCounter = 0;
                    }
                })

                const chatParticipantsSnapshotArray = await admin.firestore().collectionGroup(`chat_participants`).where('user_id', '==', userID).get()

                chatParticipantsSnapshotArray.forEach(snapshot => {
                    batchArray[batchIndex].update(snapshot.ref, {
                        'user_name': userName,
                        'first_name': firstName,
                        'last_name': lastName,
                        'search_keywords': searchKeywords,
                        'profile_pic': profilePic
                    })
                    operationCounter++;

                    if (operationCounter === 499) {
                        batchArray.push(admin.firestore().batch());
                        batchIndex++;
                        operationCounter = 0;
                    }
                })

                const warRoomMessagesSnapshotArray = await admin.firestore().collectionGroup(`war_room_messages`).where('user_id', '==', userID).get()

                warRoomMessagesSnapshotArray.forEach(snapshot => {
                    batchArray[batchIndex].update(snapshot.ref, {
                        'user_name': userName,
                        'first_name': firstName,
                        'last_name': lastName,
                        'profile_pic': profilePic
                    })
                    operationCounter++;

                    if (operationCounter === 499) {
                        batchArray.push(admin.firestore().batch());
                        batchIndex++;
                        operationCounter = 0;
                    }
                })

                const teamLeadersApplicationsSnapshotArray = await admin.firestore().collectionGroup(`team_leaders_applications`).where('user_id', '==', userID).get()

                teamLeadersApplicationsSnapshotArray.forEach(snapshot => {
                    batchArray[batchIndex].update(snapshot.ref, {
                        'user_name': userName,
                        'first_name': firstName,
                        'last_name': lastName,
                        'search_keywords': searchKeywords,
                        'profile_pic': profilePic
                    })
                    operationCounter++;

                    if (operationCounter === 499) {
                        batchArray.push(admin.firestore().batch());
                        batchIndex++;
                        operationCounter = 0;
                    }
                })

                const teamMembersApplicationsSnapshotArray = await admin.firestore().collectionGroup(`team_members_applications`).where('user_id', '==', userID).get()

                teamMembersApplicationsSnapshotArray.forEach(snapshot => {
                    batchArray[batchIndex].update(snapshot.ref, {
                        'user_name': userName,
                        'first_name': firstName,
                        'last_name': lastName,
                        'search_keywords': searchKeywords,
                        'profile_pic': profilePic
                    })
                    operationCounter++;

                    if (operationCounter === 499) {
                        batchArray.push(admin.firestore().batch());
                        batchIndex++;
                        operationCounter = 0;
                    }
                })

                const newUserDoc = (await admin.firestore().doc(`users/${userID}`).get()).data()

                const newUser = {
                    'first_name': firstName,
                    'last_name': lastName,
                    'is_verified': Object(newUserDoc)['is_verified'],
                    'profile_pic': profilePic,
                    'rep': Object(newUserDoc)['rep'],
                    'user_id': Object(newUserDoc)['user_id'],
                    'user_name': userName
                }

                const teamsSnapshotArray = await admin.firestore().collectionGroup(`teams`).where('members', 'array-contains', userID).get()

                teamsSnapshotArray.forEach(snapshot => {
                    if (Object(snapshot.data())["team_leader"]["user_id"] === userID) {
                        newUser.rep = snapshot.data()["team_leader"]["rep"]
                        batchArray[batchIndex].update(snapshot.ref, {
                            'team_leader': newUser
                        })
                        operationCounter++;

                        if (operationCounter === 499) {
                            batchArray.push(admin.firestore().batch());
                            batchIndex++;
                            operationCounter = 0;
                        }
                    } else {
                        const teamMembers: any[] = Object(snapshot.data())["team_members"]

                        let found = false
                        let i = 0
                        for (i; i < teamMembers.length; i++) {
                            if (teamMembers[i]["user_id"] === userID) {
                                found = true
                                break;
                            }
                        }

                        if (found) {
                            newUser["rep"] = teamMembers[i].rep
                            teamMembers.splice(i, 1)
                            teamMembers.push(newUser)

                            batchArray[batchIndex].update(snapshot.ref, {
                                'team_members': teamMembers
                            })
                            operationCounter++;

                            if (operationCounter === 499) {
                                batchArray.push(admin.firestore().batch());
                                batchIndex++;
                                operationCounter = 0;
                            }
                        }
                    }
                })

                const winnersSnapshotArray = await admin.firestore()
                    .collection(`wars`)
                    .where('war_participants', 'array-contains', userID)
                    .where('war_status', '==', 'RESULTS')
                    .get()

                winnersSnapshotArray.forEach(snapshot => {
                    if (Object(snapshot.data())["winner_team"]["team_leader"]["user_id"] === userID) {
                        const winnerTeam = Object(snapshot.data())["winner_team"]
                        newUser.rep = Object(snapshot.data())["winner_team"]["team_leader"]["rep"]

                        winnerTeam.team_leader = newUser

                        batchArray[batchIndex].update(snapshot.ref, {
                            'winner_team': winnerTeam
                        })
                        operationCounter++;

                        if (operationCounter === 499) {
                            batchArray.push(admin.firestore().batch());
                            batchIndex++;
                            operationCounter = 0;
                        }
                    } else if (Object(snapshot.data())["winner_team"]["members"] !== null) {
                        if (Object(snapshot.data())["winner_team"]["members"].includes(userID)) {
                            const winnerTeam = Object(snapshot.data())["winner_team"]
                            const teamMembers: any[] = Object(snapshot.data())["winner_team"]["team_members"]

                            let found = false
                            let i = 0
                            for (i; i < teamMembers.length; i++) {
                                if (teamMembers[i]["user_id"] === userID) {
                                    found = true
                                    break;
                                }
                            }

                            if (found) {
                                newUser["rep"] = teamMembers[i].rep
                                teamMembers.splice(i, 1)
                                teamMembers.push(newUser)

                                winnerTeam.team_members = teamMembers

                                batchArray[batchIndex].update(snapshot.ref, {
                                    'winner_team': winnerTeam
                                })
                                operationCounter++;

                                if (operationCounter === 499) {
                                    batchArray.push(admin.firestore().batch());
                                    batchIndex++;
                                    operationCounter = 0;
                                }
                            }
                        }
                    }
                })

                batchArray.forEach(async batch => await batch.commit());
            }

            return { 'result': true }
        } catch (e) {
            console.log(e)
            return { 'result': false }
        }
    } else {
        return { "result": false }
    }
})