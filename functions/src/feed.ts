import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

export const followSuggestions = functions.https.onCall(async (data, context) => {
    const runCount = data.run_count
    if (runCount > 1) {
        const last_doc_followers_count = data.followers_count
        return admin.firestore().collection('users')
            .orderBy('followers_count', "desc")
            .startAfter(last_doc_followers_count)
            .limit(10)
            .get()
            .then((snapshot) => {
                return snapshot.docs.map(doc => doc.data())
            })
    } else {
        return admin.firestore().collection('users')
            .orderBy('followers_count', "desc")
            .limit(10)
            .get()
            .then((snapshot) => {
                return snapshot.docs.map(doc => doc.data())
            })
    }

})

export const groupSuggestions = functions.https.onCall(async (data, context) => {
    const runCount = data.run_count
    if (runCount > 1) {
        const last_doc_members_count = data.members_count
        const groups: any[] = []
        await admin.firestore().collection('groups')
            .orderBy('members_count', "desc")
            .startAfter(last_doc_members_count)
            .limit(10)
            .get()
            .then((snapshot) => {
                snapshot.docs.map(doc => groups.push(doc.data()))
            })
        return groups
    } else {
        const groups: any[] = []
        await admin.firestore().collection('groups')
            .orderBy('members_count', "desc")
            .limit(10)
            .get()
            .then((snapshot) => {
                snapshot.docs.map(doc => groups.push(doc.data()))
            })

        return groups
    }
})

//TODO job : remove from feed after midnight : docs older than 28 days from groups and user feed data
export const feed = functions.https.onCall(async (data, context) => {
    const userID = context?.auth?.uid
    const runCount = data.run_count
    const docLimit = 20 //TODO decide if to change this to 100 OR any higher number because this does not affect number of reads
    try {
        if (runCount === 1) {
            //check if feed generated for today, else generate
            const following: String[] = []
            const groups: String[] = []
            let posts: Map<any, any>[] = []

            const currentTime = new Date()
            const today = new Date(
                currentTime.getFullYear(),
                currentTime.getMonth(),
                currentTime.getDate(),
                0,
                0,
                0,
                0
            )

            return admin.firestore()
                .doc(`users/${userID}/private_data/feed`)
                .get()
                .then(async feedSnapshot => {
                    if (!feedSnapshot.exists || feedSnapshot.data()?.create_date_time.toDate() <= today) {
                        //create new feed for the day
                        const promises = []

                        promises.push(admin.firestore()
                            .doc(`users/${userID}/private_data/following`)
                            .get()
                            .then(snapshot => {
                                snapshot.data()?.user_ids.forEach((user_id: String) => {
                                    following.push(user_id)
                                });
                            }))

                        promises.push(admin.firestore()
                            .doc(`users/${userID}/private_data/groups`)
                            .get()
                            .then(snapshot => {
                                snapshot.data()?.group_ids.forEach((group_id: String) => {
                                    groups.push(group_id)
                                });
                            }))

                        await Promise.all(promises)

                        const promises1 = []

                        let i = 0

                        for (i = 0; i < following.length; i++) {
                            promises1.push(admin.firestore()
                                .doc(`users_feed_data/${following[i]}`)
                                .get()
                                .then(snapshot => {
                                    snapshot.data()?.last_28_d.forEach((post: Map<any, any>) => {
                                        posts.push(post)
                                    });
                                }))
                        }

                        for (i = 0; i < groups.length; i++) {
                            promises1.push(admin.firestore()
                                .doc(`groups_feed_data/${groups[i]}`)
                                .get()
                                .then(snapshot => {
                                    snapshot.data()?.last_28_d.forEach((post: Map<any, any>) => {
                                        posts.push(post)
                                    });
                                }))
                        }

                        await Promise.all(promises1)

                        //Take only top 300 posts - same limit in else block, onFollow, onMemberAddedToGroup functions too
                        posts = posts.slice(0, 300)

                        await admin.firestore().doc(`users/${userID}/private_data/feed`).set(
                            {
                                'posts_count': posts.length,
                                'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
                                'last_new_run_date_time': admin.firestore.FieldValue.serverTimestamp(),
                                'run_count': 1,
                                'posts': posts
                            }
                        )

                        posts = posts.sort((a, b) => Object(b)["create_date_time"] - Object(a)["create_date_time"])

                        return posts.slice(0, docLimit)
                    } else {
                        //append new posts after the last new run timestamp

                        let last_new_run_date_time: number = 0

                        const promises = []

                        promises.push(admin.firestore().doc(`users/${userID}/private_data/feed`)
                            .get()
                            .then(snapshot => {
                                last_new_run_date_time = snapshot.data()?.last_new_run_date_time.toDate()
                                snapshot.data()?.posts.forEach((post: Map<any, any>) => {
                                    posts.push(post)
                                });
                            }))

                        promises.push(admin.firestore()
                            .doc(`users/${userID}/private_data/following`)
                            .get()
                            .then(snapshot => {
                                snapshot.data()?.user_ids.forEach((user_id: String) => {
                                    following.push(user_id)
                                });
                            }))

                        promises.push(admin.firestore()
                            .doc(`users/${userID}/private_data/groups`)
                            .get()
                            .then(snapshot => {
                                snapshot.data()?.group_ids.forEach((group_id: String) => {
                                    groups.push(group_id)
                                });
                            }))

                        await Promise.all(promises)

                        const promises1 = []
                        const following_arrays = []
                        const groups_arrays = []
                        const new_posts: any[] = []

                        let i
                        if (following.length > 0)
                            for (i = 0; i < following.length % 10 + 1; i++) {
                                following_arrays.push(following.splice(10 * i, 10 * (i + 1)))
                            }

                        if (groups.length > 0)
                            for (i = 0; i < groups.length % 10 + 1; i++) {
                                groups_arrays.push(groups.splice(10 * i, 10 * (i + 1)))
                            }

                        for (i = 0; i < following_arrays.length; i++) {
                            promises1.push(admin.firestore()
                                .collection(`users_feed_data`)
                                .where('user_id', 'in', following_arrays[i])
                                .where('last_updated', '>=', last_new_run_date_time)
                                .get()
                                .then(querySnapshot => {
                                    querySnapshot.docs.forEach(doc => {
                                        const doc_posts: Map<any, any>[] = doc.data()?.last_28_d
                                        let j
                                        for (j = 0; j < doc_posts.length; j++) {
                                            if (Object(doc_posts[j])["create_date_time"] > last_new_run_date_time) {
                                                new_posts.push(doc_posts[j])
                                            }
                                        }
                                    })
                                }))
                        }

                        for (i = 0; i < groups_arrays.length; i++) {
                            promises1.push(admin.firestore()
                                .collection(`groups_feed_data`)
                                .where('group_id', 'in', groups_arrays[i])
                                .where('last_updated', '>=', last_new_run_date_time)
                                .get()
                                .then(querySnapshot => {
                                    querySnapshot.docs.forEach(doc => {
                                        const doc_posts: Map<any, any>[] = doc.data()?.last_28_d
                                        let j
                                        for (j = 0; j < doc_posts.length; j++) {
                                            if (Object(doc_posts[j])["create_date_time"] > last_new_run_date_time) {
                                                new_posts.push(doc_posts[j])
                                            }
                                        }
                                    })
                                }))
                        }

                        await Promise.all(promises1)

                        new_posts.forEach(new_post => {
                            posts.push(new_post)
                        })

                        posts = posts.slice(0, 300)

                        await admin.firestore().doc(`users/${userID}/private_data/feed`).set(
                            {
                                'posts_count': posts.length,
                                'last_new_run_date_time': admin.firestore.FieldValue.serverTimestamp(),
                                'run_count': 1,
                                'posts': posts
                            },
                            { merge: true }
                        )

                        posts = posts.sort((a, b) => Object(b)["create_date_time"] - Object(a)["create_date_time"])

                        return posts.slice(0, docLimit)
                    }
                })
                .catch((error) => {
                    console.log(error)
                    return null
                })
        } else {
            //TODO get last doc id - start after this because on follow new posts will be added and that may lead to repititive items in feed
            //TODO ensure only unique elements are present - because on follow last new run date time is not updated and hence when this is run, duplicates will be found
            //display from pre-generated feed
            let posts: Map<any, any>[] = []

            await admin.firestore().doc(`users/${userID}/private_data/feed`)
                .get()
                .then(snapshot => {
                    snapshot.data()?.posts.forEach((post: Map<any, any>) => {
                        posts.push(post)
                    });
                })
                .catch((error) => {
                    console.log(error)
                    return null
                })

            posts = posts.sort((a, b) => Object(b)["create_date_time"] - Object(a)["create_date_time"])

            return posts.slice(docLimit * (runCount - 1), docLimit * runCount)
        }
    } catch (e) {
        console.log(e)
        return null
    }
})

export const exploreFeed = functions.https.onCall(async (data, context) => {
    const userID = context?.auth?.uid
    const runCount = data.run_count
    const category = data.category
    const posts: any[] = []
    const promises: any[] = []
    let snapshot: FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentData>

    if (userID) {
        if (runCount === 1) {
            if (category === 'Trending') {
                snapshot = await admin.firestore().collection('posts')
                    .orderBy('post_id', 'asc')
                    .orderBy('create_date_time', "desc")
                    .limit(20)
                    .get()
            } else {
                snapshot = await admin.firestore().collection('posts')
                    .orderBy('post_id', 'asc')
                    .orderBy('create_date_time', "desc")
                    .where('category', '==', category)
                    .limit(20)
                    .get()
            }
        } else {
            const last_doc_id = data.post_id
            if (category === 'Trending') {
                snapshot = await admin.firestore().collection('posts')
                    .orderBy('post_id', 'asc')
                    .orderBy('create_date_time', "desc")
                    .startAfter(last_doc_id)
                    .limit(20)
                    .get()
            } else {
                snapshot = await admin.firestore().collection('posts')
                    .orderBy('post_id', 'asc')
                    .orderBy('create_date_time', "desc")
                    .where('category', '==', category)
                    .startAfter(last_doc_id)
                    .limit(20)
                    .get()
            }
        }

        snapshot.docs.map(doc => posts.push(doc.data()))

        posts.forEach((post, i) => {
            Object(post)["post_id"] = snapshot.docs[i].ref.id
            const postID = Object(post)["post_id"]

            Object(post)["create_date_time"] = Object(post)["create_date_time"].toDate().getTime()

            promises.push(
                admin.firestore().collection(`posts/${postID}/reacts`)
                    .where('user_id', '==', userID)
                    .get()
                    .then(querySnapshot => {
                        if (querySnapshot.docs.length !== 0)
                            Object(post)["is_reacted"] = true
                        else
                            Object(post)["is_reacted"] = false
                    })
            )

            promises.push(
                admin.firestore().collection(`posts/${postID}/react_shards`)
                    .get()
                    .then(querySnapshot => {
                        let react_count: number = 0

                        querySnapshot.docs.forEach(doc => {
                            react_count += Object(doc.data())["count"]
                        })

                        Object(post)["react_count"] = react_count
                    })
            )

            promises.push(
                admin.firestore().collection(`posts/${postID}/comment_shards`)
                    .get()
                    .then(querySnapshot => {
                        let comments_count: number = 0

                        querySnapshot.docs.forEach(doc => {
                            comments_count += Object(doc.data())["count"]
                        })

                        Object(post)["comments_count"] = comments_count
                    })
            )
        })

        await Promise.all(promises)

        return posts
    } else {
        return null
    }
})