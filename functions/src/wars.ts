import * as functions from 'firebase-functions';
import admin = require('firebase-admin');

export const onWarCreated = functions.firestore.document('wars/{warID}')
    .onCreate(async (snap, context) => {
        const warID = context.params.warID;

        const newWar = snap.data()
        if (newWar) {
            let group_name = ""
            let war_name = ""
            let war_pic = ""
            let card_color = "#f44336"

            const war = snap.data()

            war_name = Object(war)["war_name"]
            const group_id = Object(war)["group_id"]
            group_name = Object(war)["group_name"]
            war_pic = Object(war)["war_pic"]
            card_color = Object(war)["card_color"]

            const today: Date = new Date()

            const end_date_times: Date[] =
                [new Date(`${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()} 00:00:00`),
                new Date(`${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()} 03:00:00`),
                new Date(`${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()} 06:00:00`),
                new Date(`${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()} 09:00:00`),
                new Date(`${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()} 12:00:00`),
                new Date(`${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()} 15:00:00`),
                new Date(`${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()} 18:00:00`),
                new Date(`${today.getMonth() + 1}/${today.getDate()}/${today.getFullYear()} 21:00:00`),
                ]

            const create_date_time: Date = newWar.start_date_time.toDate()

            let i

            for (i = 0; i < end_date_times.length; i++) {
                if (end_date_times[i] > create_date_time)
                    break
            }

            const team_leaders_selection_end_time = new Date(
                create_date_time.getFullYear(),
                create_date_time.getMonth(),
                create_date_time.getDate(),
                end_date_times[i].getHours(),
                0,
                0,
                0
            )

            team_leaders_selection_end_time.setTime(
                team_leaders_selection_end_time.getTime() + (9 * 60 * 60 * 1000)
            )

            const team_members_selection_end_time = new Date(team_leaders_selection_end_time)

            team_members_selection_end_time.setTime(
                team_members_selection_end_time.getTime() + (18 * 60 * 60 * 1000)
            )

            const d_day_end_time = new Date(team_members_selection_end_time)

            d_day_end_time.setTime(
                d_day_end_time.getTime() + (24 * 60 * 60 * 1000)
            )

            const war_end_time = new Date(d_day_end_time)

            war_end_time.setTime(
                war_end_time.getTime() + (24 * 60 * 60 * 1000)
            )

            await admin.firestore().doc(`wars/${warID}`)
                .update({
                    'team_leaders_selection_end_time': admin.firestore.Timestamp.fromDate(team_leaders_selection_end_time),
                    'team_members_selection_end_time': admin.firestore.Timestamp.fromDate(team_members_selection_end_time),
                    'd_day_end_time': admin.firestore.Timestamp.fromDate(d_day_end_time),
                    'war_end_time': admin.firestore.Timestamp.fromDate(war_end_time)
                })

            await admin.firestore().doc(`groups/${group_id}`)
                .update({
                    'wars_count': admin.firestore.FieldValue.increment(1),
                    'wars_left_this_month': admin.firestore.FieldValue.increment(-1),
                    'war_name': war_name,
                    'war_pic': war_pic,
                    'war_status': 'TEAM LEADERS SELECTION',
                    'war_id': warID,
                    'card_color': card_color,
                    'is_war_active': true,
                })

            const payload: admin.messaging.MessagingPayload = {
                notification: {
                    title: war_name,
                    body: `Memers Assemble!\n${group_name} has started a Meme War`,
                    clickAction: 'FLUTTER_NOTIFICATION_CLICK'
                },
                data: {
                    "type": "war",
                    "id": warID
                }
            }

            return admin.messaging().sendToTopic("group-" + group_id, payload)
        } else {
            return null
        }
    })

export const onTeamLeaderApplicationCreated = functions.firestore.document('wars/{warID}/team_leaders_applications/{applicationID}')
    .onCreate(async (snapshot, context) => {
        const warID = context.params.warID
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`wars/${warID}/team_leaders_application_shards/${shardId.toString()}`);
        return shardRef.set({ count: admin.firestore.FieldValue.increment(1) }, { merge: true });
    })

export const onTeamMemberApplicationCreated = functions.firestore.document('wars/{warID}/team_members_applications/{applicationID}')
    .onCreate(async (snapshot, context) => {
        const warID = context.params.warID
        const shardId = Math.floor(Math.random() * 10);
        const shardRef = admin.firestore().doc(`wars/${warID}/team_members_application_shards/${shardId.toString()}`);
        return shardRef.set({ count: admin.firestore.FieldValue.increment(1) }, { merge: true });
    })

export const onTeamLeaderSelected = functions.firestore.document('wars/{warID}/teams/{teamID}')
    .onCreate(async (snapshot, context) => {
        const warID = context.params.warID
        let warName = ""
        const teamLeaderID = Object(snapshot.data())["team_leader"]["user_id"]

        //TODO can make this efficient by including war name in each team doc
        await admin.firestore().doc(`wars/${warID}`).get().then(doc => {
            warName = Object(doc.data())["war_name"]
        })

        const tokens = (await admin.firestore().collection(`users/${teamLeaderID}/device_tokens`).get()).docs.map(token => token.id)

        const notification_body = `Congratulations!!🎉\nYou are selected as a Team Leader for ${warName}`

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: "Meme War",
                body: notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "war",
                "id": warID
            }
        }

        return admin.messaging().sendToDevice(tokens, payload)
    })

//Team members receive this notification only as approval
export const onChatParticipantAdded = functions.firestore.document('war_rooms/{warRoomID}/chat_participants/{participantID}')
    .onCreate(async (snaphsot, context) => {
        const warRoomID = context.params.warRoomID
        let warName = ""
        const participantID = context.params.participantID

        await admin.firestore().doc(`war_rooms/${warRoomID}`).get().then(doc => {
            warName = Object(doc.data())["war_name"]
        })

        const tokens = (await admin.firestore().collection(`users/${participantID}/device_tokens`).get()).docs.map(token => token.id)

        const notification_body = `Added to war room for ${warName}!\nYou are in the war Now ⚔️`

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: warName,
                body: notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "war_room",
                "id": warRoomID
            }
        }

        await admin.messaging().subscribeToTopic(tokens, `war_room-${warRoomID}`)

        await updateBadgesForWarrior(participantID, tokens)

        return admin.messaging().sendToDevice(tokens, payload)
    })

export const onWarRoomMessageSent = functions.firestore.document(`war_rooms/{warRoomID}/war_room_messages/{messageID}`)
    .onCreate(async (snapshot, context) => {
        const warRoomID = context.params.warRoomID

        const warRoom = await admin.firestore().doc(`war_rooms/${warRoomID}`).get()
        const teamName = Object(warRoom.data())["team_name"]

        const notification_body = Object(snapshot.data())["type"] == "text_message" ?
            Object(snapshot.data())["user_name"] + " : " + Object(snapshot.data())["content"]
            : Object(snapshot.data())["user_name"] + " : Image"

        const payload: admin.messaging.MessagingPayload = {
            notification: {
                title: teamName,
                body: notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "war_room",
                "id": warRoomID
            }
        }

        return admin.messaging().sendToTopic("war_room-" + warRoomID, payload)
    })

//TODO badges
export const warStatusUpdater = functions.pubsub
    .schedule('0 */3 * * *')
    .timeZone('GMT')
    .onRun(async context => {
        const currentTime = new Date()
        const scheduledTime = new Date(
            currentTime.getFullYear(),
            currentTime.getMonth(),
            currentTime.getDate(),
            currentTime.getHours(),
            0,
            0,
            0
        )

        console.log(scheduledTime.toString())

        const promises = []

        promises.push(admin.firestore().collection('wars')
            .where('war_status', '==', 'TEAM LEADERS SELECTION')
            .where('team_leaders_selection_end_time', '==', scheduledTime)
            .get()
            .then(async querySnapshot => {
                querySnapshot.forEach(async (snapshot) => {
                    if (Object(snapshot.data())["are_team_leaders_selected"]) {
                        const teamLeadersSelectionUpdateBatch = admin.firestore().batch()

                        teamLeadersSelectionUpdateBatch.update(snapshot.ref, { 'war_status': 'TEAM MEMBERS SELECTION' })

                        const groupID = snapshot.data().group_id

                        teamLeadersSelectionUpdateBatch.update(
                            admin.firestore()
                                .doc(`groups/${groupID}`),
                            { 'war_status': 'TEAM MEMBERS SELECTION' }
                        )

                        await teamLeadersSelectionUpdateBatch.commit()

                        const payload: admin.messaging.MessagingPayload = {
                            notification: {
                                title: snapshot.data()["war_name"],
                                body: `Team members selection has started!\nApply now!`,
                                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
                            },
                            data: {
                                "type": "war",
                                "id": snapshot.id
                            }
                        }

                        return admin.messaging().sendToTopic("group-" + groupID, payload)
                    } else {
                        //TODO add this in faqs
                        const warDismissBatch = admin.firestore().batch()

                        warDismissBatch.update(
                            snapshot.ref,
                            {
                                'war_status': 'WAR DISMISSED',
                                'is_war_active': false,
                            })

                        const groupID = snapshot.data().group_id

                        warDismissBatch.update(
                            admin.firestore()
                                .doc(`groups/${groupID}`),
                            {
                                'is_war_active': false,
                                'war_name': "",
                                'war_id': "",
                                'card_color': "",
                                'war_pic': "",
                                'war_status': ""
                            }
                        )

                        await warDismissBatch.commit()

                        const payload: admin.messaging.MessagingPayload = {
                            notification: {
                                title: snapshot.data()["war_name"],
                                body: `No team leaders selected!\nWar dismissed😔`,
                                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
                            },
                            data: {
                                "type": "war",
                                "id": snapshot.id
                            }
                        }

                        return admin.messaging().sendToTopic("group-" + groupID, payload)
                    }
                })
            }))

        promises.push(admin.firestore().collection('wars')
            .where('war_status', '==', 'TEAM MEMBERS SELECTION')
            .where('team_members_selection_end_time', '==', scheduledTime)
            .get()
            .then(async querySnapshot => {
                querySnapshot.forEach(async (snapshot) => {
                    const teams = await admin.firestore()
                        .collection(`wars/${snapshot.id}/teams`)
                        .get();

                    const members: Array<String> = []

                    teams.docs.forEach((team) => {
                        Object(team.data())["members"].forEach((member: String) => {
                            members.push(member)
                        });
                    })

                    const teamMembersSelectionUpdateBatch = admin.firestore().batch()

                    teamMembersSelectionUpdateBatch.update(
                        admin.firestore()
                            .doc(`wars/${snapshot.id}`), { "war_participants": members, "war_status": 'D-DAY' }
                    )

                    const groupID = snapshot.data().group_id

                    teamMembersSelectionUpdateBatch.update(
                        admin.firestore()
                            .doc(`groups/${groupID}`), { "war_status": 'D-DAY' }
                    )

                    await teamMembersSelectionUpdateBatch.commit()

                    const payload: admin.messaging.MessagingPayload = {
                        notification: {
                            title: snapshot.data()["war_name"],
                            body: `It's D-Day ⚔️\nMeme Up!`,
                            clickAction: 'FLUTTER_NOTIFICATION_CLICK'
                        },
                        data: {
                            "type": "war",
                            "id": snapshot.id
                        }
                    }

                    return admin.messaging().sendToTopic("group-" + groupID, payload)
                })
            }))

        promises.push(admin.firestore().collection('wars')
            .where('war_status', '==', 'D-DAY')
            .where('d_day_end_time', '==', scheduledTime)
            .get()
            .then(async querySnapshot => {
                querySnapshot.forEach(async (war) => {
                    const teams = await admin.firestore()
                        .collection(`wars/${war.id}/teams`)
                        .get()

                    let max_react_count_for_team = 0
                    let max_react_count_team: FirebaseFirestore.DocumentData = teams.docs[0]

                    let max_react_count_for_post = 0
                    let max_react_count_post = ""
                    let max_react_count_post_owner_userID = ""

                    for (const team of teams.docs) {
                        let total_react_count_for_team = 0

                        const posts = await admin.firestore()
                            .collection('posts')
                            .where('team_id', '==', team.id)
                            .get()

                        for (const post of posts.docs) {
                            let react_count_for_post = 0
                            const react_shards = await admin.firestore()
                                .collection(`posts/${post.id}/react_shards`)
                                .get()

                            react_shards.forEach((react_shard) => {
                                total_react_count_for_team += Object(react_shard.data())["count"]
                                react_count_for_post += Object(react_shard.data())["count"]
                            })

                            if (react_count_for_post > max_react_count_for_post) {
                                max_react_count_for_post = react_count_for_post
                                max_react_count_post = post.id
                                max_react_count_post_owner_userID = post.data()["user_id"]
                            }
                        }

                        if (total_react_count_for_team > max_react_count_for_team) {
                            max_react_count_for_team = total_react_count_for_team
                            max_react_count_team = team
                        }

                    }

                    const team_members: any[] = []

                    for (let i = 0; i < Object(max_react_count_team.data())["members_count"] - 1; i++) {
                        team_members.push(Object(max_react_count_team.data())["team_members"][i.toString()])
                    }

                    const winner_team = {
                        "members_count": Object(max_react_count_team.data())["members_count"],
                        "team_id": max_react_count_team.id,
                        "team_leader": Object(max_react_count_team.data())["team_leader"],
                        "team_members": team_members,
                        "members": Object(max_react_count_team.data())["members"],
                        "team_name": Object(max_react_count_team.data())["team_name"],
                        "team_pic": Object(max_react_count_team.data())["team_pic"],
                        "team_rep_score": Object(max_react_count_team.data())["team_rep_score"]
                    }

                    const dDayUpdateBatch = admin.firestore().batch()

                    dDayUpdateBatch.update(
                        admin.firestore()
                            .doc(`wars/${war.id}`), {
                        "winner_team": winner_team,
                        "meme_of_the_war": max_react_count_post,
                        'war_status': 'RESULTS'
                    })

                    const groupID = war.data().group_id

                    dDayUpdateBatch.update(
                        admin.firestore()
                            .doc(`groups/${groupID}`), { 'war_status': 'RESULTS', }
                    )

                    await dDayUpdateBatch.commit()

                    const memeOfTheWarNotificationBody = "Congratulations!!🎉\nYou've secured Meme Of The War!"

                    const memeOfTheWarWinnerPayload: admin.messaging.MessagingPayload = {
                        notification: {
                            title: war.data()["war_name"],
                            body: memeOfTheWarNotificationBody,
                            clickAction: 'FLUTTER_NOTIFICATION_CLICK'
                        },
                        data: {
                            "type": "war",
                            "id": war.id
                        }
                    }

                    const tokens_for_meme_of_the_war_winner = (
                        await admin.firestore()
                            .collection(`users/${max_react_count_post_owner_userID}/device_tokens`)
                            .get()
                    ).docs.map(token => token.id)

                    await admin.messaging().sendToDevice(tokens_for_meme_of_the_war_winner, memeOfTheWarWinnerPayload)

                    await admin.firestore().collection(`users/${max_react_count_post_owner_userID}/notifications`).add({
                        'type': 'war',
                        'id': war.id,
                        'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
                        'message': memeOfTheWarNotificationBody,
                        "leading_image": "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fmeme_of_the_war.png?alt=media"
                    })

                    await admin.firestore().doc(`users/${max_react_count_post_owner_userID}/private_data/notification`).set(
                        {
                            'last_notification_time': admin.firestore.FieldValue.serverTimestamp()
                        }, { merge: true }
                    )

                    await admin.firestore().doc(`users/${max_react_count_post_owner_userID}`)
                        .set({
                            'rep': admin.firestore.FieldValue.increment(0.2)
                        }, { merge: true })

                    const warWinnerNotificationBody = "Congratulations!!🎉\nYou've won the war!"

                    const warWinnersPayload: admin.messaging.MessagingPayload = {
                        notification: {
                            title: war.data()["war_name"],
                            body: warWinnerNotificationBody,
                            clickAction: 'FLUTTER_NOTIFICATION_CLICK'
                        },
                        data: {
                            "type": "war",
                            "id": war.id
                        }
                    }

                    for (const memberUserId of Object(max_react_count_team.data())["members"]) {
                        const tokensForWarWinners = (
                            await admin.firestore()
                                .collection(`users/${memberUserId}/device_tokens`)
                                .get()
                        ).docs.map(token => token.id)

                        await admin.messaging().sendToDevice(tokensForWarWinners, warWinnersPayload)

                        await admin.firestore().collection(`users/${memberUserId}/notifications`).add({
                            'type': 'war',
                            'id': war.id,
                            'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
                            'message': warWinnerNotificationBody,
                            "leading_image": "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fwar_winner_medal.png?alt=media"
                        })

                        await admin.firestore().doc(`users/${memberUserId}/private_data/notification`).set(
                            {
                                'last_notification_time': admin.firestore.FieldValue.serverTimestamp()
                            }, { merge: true }
                        )

                        await updateBadgesForWarWinners(memberUserId, tokensForWarWinners)
                    }

                    const payload: admin.messaging.MessagingPayload = {
                        notification: {
                            title: war.data()["war_name"],
                            body: `Results Out!\nRespect for the winners!`,
                            clickAction: 'FLUTTER_NOTIFICATION_CLICK'
                        },
                        data: {
                            "type": "war",
                            "id": war.id
                        }
                    }

                    return admin.messaging().sendToTopic("group-" + groupID, payload)
                })
            }))

        promises.push(admin.firestore().collection('wars')
            .where('war_status', '==', 'RESULTS')
            .where('war_end_time', '==', scheduledTime)
            .get()
            .then(async querySnapshot => {
                querySnapshot.forEach(async (snapshot) => {
                    const groupID = snapshot.data().group_id

                    const resultsUpdateBatch = admin.firestore().batch()

                    resultsUpdateBatch.update(
                        admin.firestore()
                            .doc(`groups/${groupID}`),
                        {
                            'is_war_active': false,
                            'war_name': "",
                            'war_id': "",
                            'card_color': "",
                            'war_pic': "",
                            'war_status': ""
                        }
                    )

                    resultsUpdateBatch.update(
                        admin.firestore()
                            .doc(`wars/${snapshot.id}`),
                        {
                            'is_war_active': false,
                        }
                    )

                    return resultsUpdateBatch.commit()

                })
            }))

        return Promise.all(promises)
    })

const updateBadgesForWarrior = async (userID: string, tokens: string[]) => {
    await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`)
        .set({
            'total_wars': admin.firestore.FieldValue.increment(1),
            'season_wars': admin.firestore.FieldValue.increment(1)
        }, { merge: true })

    //Badge update functionality
    const badge_rep_data = (await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`).get()).data()

    const season_wars = Object(badge_rep_data)["season_wars"]

    await admin.firestore().doc(`users/${userID}`)
        .set({
            'rep': admin.firestore.FieldValue.increment(0.1)
        }, { merge: true })


    let is_badge_updated = false
    let new_badge_notification_body = ""

    if (season_wars === 3) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fnewbie.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fwarrior.png?alt=media')
            }, { merge: true })


        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Warrior Badge!'
        is_badge_updated = true
    } else if (season_wars === 7) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fwarrior.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fknight.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Knight Badge!'
        is_badge_updated = true
    } else if (season_wars === 15) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fknight.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fbishop.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Bishop Badge!'
        is_badge_updated = true
    } else if (season_wars === 25) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fbishop.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fduke.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the Duke Badge!'
        is_badge_updated = true
    }

    if (is_badge_updated) {
        await admin.firestore().collection(`users/${userID}/notifications`).add({
            'type': 'user',
            'id': userID,
            'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
            'message': new_badge_notification_body,
            "leading_image": "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fbadge.png?alt=media"
        })

        await admin.firestore().doc(`users/${userID}/private_data/notification`).set(
            {
                'last_notification_time': admin.firestore.FieldValue.serverTimestamp()
            }, { merge: true }
        )

        const payload_badge: admin.messaging.MessagingPayload = {
            notification: {
                title: "New Badge!!",
                body: new_badge_notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "user",
                "id": userID
            }
        }

        await admin.messaging().sendToDevice(tokens, payload_badge)
    }
}

const updateBadgesForWarWinners = async (userID: string, tokens: string[]) => {
    await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`)
        .set({
            'total_wars_won': admin.firestore.FieldValue.increment(1),
            'season_wars_won': admin.firestore.FieldValue.increment(1)
        }, { merge: true })

    //Badge update functionality
    const badge_rep_data = (await admin.firestore().doc(`users/${userID}/private_data/badge_rep_data`).get()).data()

    const season_wars_won = Object(badge_rep_data)["season_wars_won"]

    await admin.firestore().doc(`users/${userID}`)
        .set({
            'rep': admin.firestore.FieldValue.increment(0.3)
        }, { merge: true })

    let is_badge_updated = false
    let new_badge_notification_body = ""

    if (season_wars_won === 1) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fnewbie.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fwar_slayer.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the War Slayer Badge!'
        is_badge_updated = true
    } else if (season_wars_won === 3) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fwar_slayer.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fwar_champ.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the War Champ Badge!'
        is_badge_updated = true
    } else if (season_wars_won === 7) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fwar_champ.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fwar_master.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the War Master Badge!'
        is_badge_updated = true
    } else if (season_wars_won === 15) {
        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayRemove('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fwar_master.png?alt=media')
            }, { merge: true })

        await admin.firestore().doc(`users/${userID}`)
            .set({
                'badges': admin.firestore.FieldValue.arrayUnion('https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fwar_guru.png?alt=media')
            }, { merge: true })

        new_badge_notification_body = 'Congratulations!!🎉\nYou got the War Guru Badge!'
        is_badge_updated = true
    }

    if (is_badge_updated) {
        await admin.firestore().collection(`users/${userID}/notifications`).add({
            'type': 'user',
            'id': userID,
            'create_date_time': admin.firestore.FieldValue.serverTimestamp(),
            'message': new_badge_notification_body,
            "leading_image": "https://firebasestorage.googleapis.com/v0/b/memecist-451e3.appspot.com/o/public%2Fbadges%2Fbadge.png?alt=media"
        })

        await admin.firestore().doc(`users/${userID}/private_data/notification`).set(
            {
                'last_notification_time': admin.firestore.FieldValue.serverTimestamp()
            }, { merge: true }
        )

        const payload_badge: admin.messaging.MessagingPayload = {
            notification: {
                title: "New Badge!!",
                body: new_badge_notification_body,
                clickAction: 'FLUTTER_NOTIFICATION_CLICK'
            },
            data: {
                "type": "user",
                "id": userID
            }
        }

        await admin.messaging().sendToDevice(tokens, payload_badge)
    }
}